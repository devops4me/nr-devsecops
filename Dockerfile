FROM python:2-alpine
LABEL maintainer="info@practical-devsecops.com"

ADD django.nv/* /app/
ADD ssh/* /root/.ssh/
WORKDIR /app
RUN apk add --no-cache gawk sed bash grep bc coreutils openrc openssh \
    && rm -rf /tmp/* /var/cache/apk/* \
    && mkdir -p /run/openrc \
    && touch /run/openrc/softlevel \
    && pip install -r requirements.txt \
    && echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config \
    && echo "PasswordAuthentication no" >> /etc/ssh/sshd_config \
    && chmod +x reset_db.sh && bash reset_db.sh \
    && rm /etc/motd \
    && echo "root:vagrant@123" | chpasswd \
    && chmod 600 /root/.ssh/id_rsa \
    && chmod 644 /root/.ssh/id_rsa.pub \
    && cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys

EXPOSE 22 8000
CMD ["/app/runapp.sh"]
